/**
 * Development server
 */
var path = require('path');
var webpack = require('webpack');
var config = require('./webpack.config.dev');
var browserSync = require('browser-sync');
var historyApiFallback = require('connect-history-api-fallback');

var compiler = webpack(config);

browserSync({
    server: {
        baseDir: 'src/demo',
        middleware: [
            require('webpack-dev-middleware')(compiler, {
                publicPath: config.output.publicPath,
                stats: { colors: true }
            }),
            require('webpack-hot-middleware')(compiler),
            historyApiFallback()
        ]
    },
    files: [
        'src/components/**/*.js',
        'src/**/*.html'
    ]
});
