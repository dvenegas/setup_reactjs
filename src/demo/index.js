/**
 * Entrypoint script demo
 */
import React from 'react';
import { render } from 'react-dom';

import RootApp from './root';

const MyApp = (() => (
    <RootApp /> // eslint-disable-line
));

render(
  <MyApp />,
  document.getElementById('root')
);
