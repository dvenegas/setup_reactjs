/**
 * Root app
 */
import React, {Component, PropTypes} from 'react';
import Box from '../components/Box';
import Footer from '../components/Footer';

class RootApp extends Component {

    constructor(props){
        super(props);
        this.clickRoot = this.clickRoot.bind(this);
        this.state = {
            color: 'white'
        }
    }

    clickRoot(elem){
        this.setState({color:elem});
    }

    render(){
        const styles = {
            width: 300,
            margin: '20px auto',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'column'
        }
        return (
            <div style={styles}>
                <Box color={this.state.color}/>
                <Footer click={this.clickRoot} />
            </div>
        );
    }
};

export default RootApp;
