/**
 * New app
 */
import React, {Component, PropTypes} from 'react';

const propTypes = {
    icon: PropTypes.string,
    txt: PropTypes.string,
    click: PropTypes.funct
}

const defaultProps = {
    icon: 'home',
    txt: 'Home'
}

class Item extends Component {
    
    constructor(props){
        super(props);
        this.clickItem = this.clickItem.bind(this);
        this.itemSet = {
            menu : {
                color: 'red',
                path: 'M2 6h28v6h-28zM2 14h28v6h-28zM2 22h28v6h-28z'
            },
            home : {
                color: 'violet',
                path: 'M32 19l-6-6v-9h-4v5l-6-6-16 16v1h4v10h10v-6h4v6h10v-10h4z'
            },
            camera : {
                color: 'blue',
                path: 'M9.5 19c0 3.59 2.91 6.5 6.5 6.5s6.5-2.91 6.5-6.5-2.91-6.5-6.5-6.5-6.5 2.91-6.5 6.5zM30 8h-7c-0.5-2-1-4-3-4h-8c-2 0-2.5 2-3 4h-7c-1.1 0-2 0.9-2 2v18c0 1.1 0.9 2 2 2h28c1.1 0 2-0.9 2-2v-18c0-1.1-0.9-2-2-2zM16 27.875c-4.902 0-8.875-3.973-8.875-8.875s3.973-8.875 8.875-8.875c4.902 0 8.875 3.973 8.875 8.875s-3.973 8.875-8.875 8.875zM30 14h-4v-2h4v2z'
            },
            calendar: {
                color: 'pink',
                path: 'M10 12h4v4h-4zM16 12h4v4h-4zM22 12h4v4h-4zM4 24h4v4h-4zM10 24h4v4h-4zM16 24h4v4h-4zM10 18h4v4h-4zM16 18h4v4h-4zM22 18h4v4h-4zM4 18h4v4h-4zM26 0v2h-4v-2h-14v2h-4v-2h-4v32h30v-32h-4zM28 30h-26v-22h26v22z'
            }
        }
    }

    clickItem(){
        this.props.click( this.itemSet[ this.props.icon ].color );
    }

    render(){
        const { txt, icon } = this.props;
        const styles = {
            box:{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                padding: '10px 0'
            },
            svg: {
                width: 32,
                height: 32,
                marginBottom: 10,
                fill: '#888'
            },
            h3:{
                margin: 0,
                fontFamily: 'sans-serif',
                fontSize: '.7em',
                color: '#888',
                textTransform: 'uppercase'
            }
        }
        return(
            <div style={styles.box} onClick={this.clickItem}>
                <svg style={styles.svg}>
                    <path d={this.itemSet[icon].path}></path>
                </svg>
                <h3 style={styles.h3}>{txt}</h3>
            </div>
        );
    }
}

export default Item;
