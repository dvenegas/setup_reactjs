/**
 * New app
 */
import React, {Component, PropTypes} from 'react';

class Box extends Component {

    constructor(props){
        super(props);
    }

    render(){
        console.log(this.props)
        const styles = {
            box:{
                padding: 10,
                boxSizing: 'border-box',
                backgroundColor: this.props.color,
                color: '#fff',
                fontFamily: 'sans-serif',
                textAlign: 'center'
            },
            title:{
                fontSize: '1.2em'
            },
            button:{
                padding: '5px 20px',
                backgroundColor: '#2288cc',
                borderRadius: 200,
                border: 'none',
                color: '#fff',
                textTransform: 'uppercase',
                fontSize: '.7em'
            }
        }
        return(
            <div style={styles.box}>
                <h1 style={styles.title}>Start a new punk rock band with my mates</h1>
                <h4>sun 25</h4>
                <button style={styles.button}>Add</button>
            </div>
        );
    }
}

export default Box;
