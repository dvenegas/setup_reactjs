/**
 * New app
 */
import React, {Component, PropTypes} from 'react';
import Item from '../Item';

const propTypes = {
    click: PropTypes.funct
}

class Footer extends Component {

    constructor(props){
        super(props);
        this.clickFooter = this.clickFooter.bind(this);
    }

    clickFooter(elem){
        this.props.click(elem);
    }

    render(){
        const styles = {
            width: '100%',
            backgroundColor: '#eee',
            display: 'flex',
            justifyContent: 'space-around',
            alignItems: 'center'
        }
        return(
            <div style={styles}>
                <Item click={this.clickFooter} icon="menu" txt="Record"/>
                <Item click={this.clickFooter} icon="calendar" txt="Date"/>
                <Item click={this.clickFooter} icon="camera" txt="Photo"/>
                <Item click={this.clickFooter} icon="home" txt="Rank"/>
            </div>
        );
    }
};

export default Footer;
