# KarmaBoard Module Feed
---
- **Dependencies**
- **How to run ?**
- **RoadMap**
- **Docs**
---

## Dependencies

** Node.js(v0.12.4) & NPM(v2.10.1)  **

* `gcc` and `g++` 4.8 or newer, or
* `clang` and `clang++` 3.4 or newer
* Python 2.6 or 2.7
* GNU Make 3.81 or newer
* libexecinfo (FreeBSD and OpenBSD only)

More info:  [Github/node](https://github.com/nodejs/node).

** Install Node.js & NPM **
```sh
$ ./configure
$ make
$ [sudo] make install
```
** Other dependencies **

- Ruby

- Sass
```sh
    gem install sass
```

- Compass
```sh
    gem install compass
```

## How to run ?

- Development Mode (static server + hot reload + test runner)

    ```sh
        npm start
    ```
    ```sh
        npm run test:watch
    ```

- Test Mode (mocha runner + lint)

    ```sh
        npm test
    ```

    ```sh
        npm run lint
    ```

- Production **(Pending ... )**

## RoadMap

- Twitter Feed > List Mode
    - Simple text
    - With/Without actions
    - Widt sentiment
    - Media visualization


- Twitter Feed > Gallery Mode
    - Simple gallery
    - Simple gallery + info


- Twitter Feed > Modal Mode
    - Simple tweet
    - Gallery Slider

## Docs **(Pending ...)**
